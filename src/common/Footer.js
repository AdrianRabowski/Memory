import React from 'react';
import packageJson from '../../package.json';
import { Icon } from 'semantic-ui-react';
import { Trans } from 'react-i18next';

class Footer extends React.Component {

    render() {
        return (
            <div className="footer-style">
                <Trans i18nKey="Version">Version</Trans>: {packageJson.version}
                <a href='https://gitlab.com/AdrianRabowski/Memory' target="_blank" rel="noopener noreferrer"><Icon color='orange' name='gitlab' size='big' link /></a>
                <a href='https://www.linkedin.com/in/adrian-rabowski/' target="_blank" rel="noopener noreferrer"><Icon color='blue' name='linkedin' size='big' link /></a>
            </div>
        )
    }

}

export default Footer;